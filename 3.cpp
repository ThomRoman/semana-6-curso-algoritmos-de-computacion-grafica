#include <windows.h>
#include <GL/glut.h>
#include <iostream>
#include <math.h>

using namespace std;
void pintarpixel(GLfloat x, GLfloat y);

GLint n;
GLfloat t;
GLint cont = 0;

struct punto
{
  int x;
  int y;
} A[20];

void inicio()
{
  glClearColor(1.0, 1.0, 1.0, 0.0);
  glMatrixMode(GL_PROJECTION);
  gluOrtho2D(-40, 40, -40, 40);
}

//Hubo problemas en el factorial por eso el programa demoraba en responder
int factorial(int n)
{
  if (n < 0)
    return 0;
  else if (n > 1)
    return n * factorial(n - 1);
  return 1;
}

float combinatoria(int n, int a)
{
  return ((float)factorial(n)) / ((float)factorial(a) * (float)factorial(n - a));
}

void eje()
{
  glLineWidth(2);
  glBegin(GL_LINES);
  glColor3f(0, 0, 1);
  glVertex3f(0, 40, -1);
  glVertex3f(0, -40, -1);
  glEnd();

  glBegin(GL_LINES);
  glColor3f(0, 1, 0);
  glVertex3f(40, 0, 0);
  glVertex3f(-40, 0, 0);
  glEnd();
  glFlush();
}

void indicarpuntos()
{
  glPointSize(5);
  glColor3f(0.1, 0.0, 1.0);
  glBegin(GL_POINTS);
  for (int i = 0; i <= n; i++)
  {
    glVertex2i(A[i].x, A[i].y);
  }
  glEnd();
  glPointSize(1);
  glColor3f(1.0, 0.0, 0.0);
  glBegin(GL_LINES);

  //hubo un error en el for(int i=0;i<=n;i++) ya que cuando i era n (tomaba el tama�o maximo del vector)
  //pero en la linea 67 vector A[i+1] llama a un valor inexistente porque i+1 supera al tama�o del vector
  //por eso se cambio a for(int i=0;i<=n-1;i++)
  for (int i = 0; i <= n - 1; i++)
  {
    glVertex2i(A[i].x, A[i].y);
    glVertex2i(A[i + 1].x, A[i + 1].y);
  }
  glEnd();
  glFlush();
}

void Bezier()
{
  GLfloat suma_x = 0;
  GLfloat suma_y = 0;
  GLfloat p = -0.1;
  //se cambio el WHILE por el DO-WHILE, ya que P no llegaba a tomar el valor de 1
  do
  {
    p = p + t;
    suma_x = 0;
    suma_y = 0;
    for (int i = 0; i <= n; i++)
    {
      suma_x = suma_x + (combinatoria(n, i) * A[i].x * pow((1 - p), n - i) * pow(p, i));
      suma_y = suma_y + (combinatoria(n, i) * A[i].y * pow((1 - p), n - i) * pow(p, i));
    }
    cout << " " << p << " | " << suma_x << " ," << suma_y << endl;
    pintarpixel(suma_x, suma_y);
  } while (p <= 1);
}

void pintarpixel(GLfloat x, GLfloat y)
{
  glPointSize(3);
  glColor3f(0.0, 0.0, 0.0);
  glBegin(GL_POINTS);
  glVertex2f(x, y);
  glEnd();
  glFlush();
}

void pantalla(void)
{
  glClearColor(1, 1, 1, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  eje();
  Bezier();
  indicarpuntos();
}

int main(int argc, char **argv)
{
  int num;
  cout << "ingrese cantidad de numeros:";
  cin >> num;
  cout << endl;
  n = num - 1;
  t = 0.1;
  cout << endl;
  for (int i = 0; i <= n; i++)
  {
    cout << "ingrese P" << i + 1 << ":" << endl;
    cout << " X" << i + 1 << ":";
    cin >> A[i].x;
    cout << " Y" << i + 1 << ":";
    cin >> A[i].y;
  }
  cout << " TABLA DE DATOS" << endl;
  cout << "------------------------" << endl;
  cout << " t | (x,y)" << endl;
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glVertex3f(2, 8, 1);
  glutInitWindowSize(600, 600);
  glutInitWindowPosition(200, 50);
  glutCreateWindow("BEZIER");
  inicio();
  glutDisplayFunc(pantalla);
  glutMainLoop();
}

// g++ -m32 -Wall -o 3.out 3.cpp -L"C:\MinGW\lib" -lglu32 -lglut32 -lopengl32 -lstdc++