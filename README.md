# Integrantes:

- Nathan Silvera Iñigo
- Angela Centeno Macalupu
- Thom Maurick Roman Aguilar
- Isaac Erick Huarancca Rivas
- Elias Gomez Flores

[Primera pregunta](1.cpp)

![1](img/1.png)

[Segunda pregunta](2.cpp)

![2](img/2.png)

[Tercera pregunta](3.cpp)

![3](img/3.png)