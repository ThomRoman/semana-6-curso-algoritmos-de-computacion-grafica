//Ejercicio 2 - Algoritmos de Computacion Grafica
//////////////////////////////////////////////////////////////////////////////////////////////////
// hermiteCubico.cpp
// Este programa dibuja un c�bico Hermite que permite al usuario controlar el control de dos extremos
// puntos y los vectores tangentes all�.
//Interacci�n:
// Presione la barra espaciadora para seleccionar un punto de control o un vector tangente.
// Presione las teclas de flecha para mover el punto de control seleccionado o cambiar el vector
//tangente.
//////////////////////////////////////////////////////////////////////////////////////////////////
//OBSERVACION: Para la adecuada compilacion y ejecucion de este programa se debe tener instalado
//las librerias GLUT y GLEW.
////////////////////////////////////////////////////////////////////////////////////////////////
#include <windows.h>
#include <stdio.h>
#include <iomanip>
#include <cmath>
#include <iostream>
#ifdef __APPLE__
#include <GL/glew.h> //Incluye GLEW para punteros de funci�n, etc.
#include <GL/freeglut.h>
#include <glext.h>
#else
#include <glew.h>
#include <freeglut.h>
#include <glext.h>
#pragma comment(lib, "glew32.lib") //Indica al compilador que use glew32.lib durante el enlace
#endif

using namespace std;

//Funcion gotoxy().- Se basa en la librer�a <windows.h>, que proporciona herramientas
//interesantes e importantes para programar en consola en Windows.Asi mismo, las
//instrucciones dentro de la funci�n "gotoxy()" est�n definidas en la cabecera,
//lo que se puede comentar al respecto de esto, es que estamos utilizando un tipo
//gen�rico de "punteros" que lo que hacen es obtener primeramente, el control de nuestra
//ventana de salida, en este caso la consola de Windows, y adem�s a partir de dos
//par�metros enteros ( x, y) asignarle una posici�n en forma de coordenada donde se
//imprimir�n los datos de salida.
//En esta ocasion es para darle mayor estetica a la tabla de datos
void gotoxy(int x, int y)
{ //para desplazar el cursor en escritura
	HANDLE hcon;
	hcon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	SetConsoleCursorPosition(hcon, dwPos);
}
static int parrafo = 15;

// Begin globals.
static int numVal = 0;				// �ndice de selecci�n actual.
static float lengthArrowLine = 2.0; // Longitud de la l�nea de flecha.
static int numVertices = 50;		// N�mero de v�rtices en c�bico.
// Puntos de Control.
static float controlPoints[2][3];
// Vectores tangentes en los puntos de control.
static float tangentVectors[2][3] = {{0.0, 10.0, 0.0}, {10.0, 0.0, 0.0}};
// Longitudes de vectores tangentes.
static float squareLengthTangent[2];
// Puntos finales de vectores tangentes.
static float endPointTangentVectors[2][3];
// Puntos finales de las l�neas de flecha.
static float endPointArrowLines[4][3];
// Fin de los globales.

// Rutina de inicializaci�n.
void setup(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0); //color de fondo
}

// Funcion para calcular puntos finales de vector tangente agregando un vector tangente al vector de
// punto de control.
void computeEndPointTangentVectors(void)
{ //Funcion para el calculo de vectores de tangente de punto final
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
			endPointTangentVectors[i][j] = controlPoints[i][j] + tangentVectors[i][j];
}

// Funcion para el calculo de puntos finales de l�nea de flecha.
void computeEndPointArrowLines(void)
{
	for (int i = 0; i < 2; i++)
	{
		squareLengthTangent[i] = tangentVectors[i][0] * tangentVectors[i][0] + tangentVectors[i][1] * tangentVectors[i][1];

		if (squareLengthTangent[i] != 0.0)
		{
			endPointArrowLines[2 * i][0] = endPointTangentVectors[i][0] - lengthArrowLine * (tangentVectors[i][0] - tangentVectors[i][1]) / sqrt(2.0 * squareLengthTangent[i]);
			endPointArrowLines[2 * i][1] = endPointTangentVectors[i][1] - lengthArrowLine * (tangentVectors[i][0] + tangentVectors[i][1]) / sqrt(2.0 * squareLengthTangent[i]);
			endPointArrowLines[2 * i][2] = 0.0;
			endPointArrowLines[2 * i + 1][0] = endPointTangentVectors[i][0] - lengthArrowLine * (tangentVectors[i][0] + tangentVectors[i][1]) / sqrt(2.0 * squareLengthTangent[i]);
			endPointArrowLines[2 * i + 1][1] = endPointTangentVectors[i][1] - lengthArrowLine * (-tangentVectors[i][0] + tangentVectors[i][1]) / sqrt(2.0 * squareLengthTangent[i]);
			endPointArrowLines[2 * i + 1][2] = 0.0;
		}
	}
}

// Funcion para el dibujo de escena, donde se especifica las caracteristas que se
//desee que esta tenga a la hora de ejecutar el programa.
void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT); //Borra pantalla
	glColor3f(0.0, 0.0, 0.0);
	int i;
	float u, H0, H1, H2, H3;
	computeEndPointTangentVectors(); // Calcular puntos finales de vector tangente.
	computeEndPointArrowLines();	 // Calcular puntos finales de flecha.

	// Dibuja los puntos de control como puntos.
	glPointSize(5.0);
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_POINTS);
	for (i = 0; i < 2; i++)
	{
		glVertex3fv(controlPoints[i]); //espeifica vertices de los puntos de control
	}
	glEnd();

	// Dibuja los vectores tangentes como flechas.
	for (i = 0; i < 2; i++)
	{

		if (squareLengthTangent[i] != 0.0)
		{
			glBegin(GL_LINES);
			//especifica los vertices
			glVertex3fv(controlPoints[i]);
			glVertex3fv(endPointTangentVectors[i]); //especifica vertice de vectores de tangente de punto final
			glVertex3fv(endPointTangentVectors[i]);
			glVertex3fv(endPointArrowLines[2 * i]); //especifica vertice de punto final de flecha lineas.
			glVertex3fv(endPointTangentVectors[i]);
			glVertex3fv(endPointArrowLines[2 * i + 1]);
			glEnd();
		}
		else
		{
			glBegin(GL_POINTS);
			glVertex3fv(controlPoints[i]);
			glEnd();
		}
	}
	// Destacar el punto de control seleccionado o el vector tangente
	glColor3f(1.0, 0.0, 0.0);
	if (numVal % 2 == 0)
	{
		glBegin(GL_POINTS);
		glVertex3fv(controlPoints[numVal / 2]);
		glEnd();
	}
	else
	{
		i = numVal / 2;
		if (squareLengthTangent[i] != 0.0)
		{ //longitud cuadrada tangente
			glBegin(GL_LINES);
			glVertex3fv(controlPoints[i]);			//vertices de los puntos de control
			glVertex3fv(endPointTangentVectors[i]); //especifica vertice de vectores de tangente de punto final
			glVertex3fv(endPointTangentVectors[i]);
			glVertex3fv(endPointArrowLines[2 * i]); //especifica vertice de punto final de flecha lineas.
			glVertex3fv(endPointTangentVectors[i]);
			glVertex3fv(endPointArrowLines[2 * i + 1]);
			glEnd();
		}
		else
		{
			glBegin(GL_POINTS);
			glVertex3fv(controlPoints[i]);
			glEnd();
		}
	}

	// Dibuja la curva c�bica como una franja de l�nea.
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_LINE_STRIP);
	for (i = 0; i <= numVertices; ++i)
	{
		// Operaciones para dibujo de la curva c�bica como una franja de l�nea.
		u = (float)i / numVertices;
		H0 = 2.0 * u * u * u - 3 * u * u + 1.0;
		H1 = -2.0 * u * u * u + 3 * u * u;
		H2 = u * u * u - 2.0 * u * u + u;
		H3 = u * u * u - u * u;
		glVertex3f(H0 * controlPoints[0][0] + H1 * controlPoints[1][0] + H2 * tangentVectors[0][0] + H3 * tangentVectors[1][0], H0 * controlPoints[0][1] + H1 * controlPoints[1][1] + H2 * tangentVectors[0][1] + H3 * tangentVectors[1][1], 0.0);
	}
	glEnd();
	glutSwapBuffers(); //se usa para intercambiar los bufferes, provocando, al igual que glFlush(),
					   //para imprimir en pantalla la nueva escena.
}

// Funcion de remodelaci�n de la ventana de OpenGL.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION); //para indicarle que vamos a modificar esta matriz  PROJECTION
	glLoadIdentity();
	glOrtho(-50.0, 50.0, -50.0, 50.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW); //Para poder modificar la matriz ModelView
	glLoadIdentity();			//Para acumularle operaciones de inicializaci�n a la identidad
}

//Funcion que permite el envio e impresion de datos del recorrido de los puntos a la tabla
//que se mostrara en pantalla.
//esta funcion agrega las nuevas posiciones de los puntos de control a la tabla de datos
void tablaDatos()
{
	//Respecto al Punto 0
	cout << setw(10) << " "
		 << "(" << controlPoints[0][0] << "," << controlPoints[0][1] << ")" << setfill(' ');
	gotoxy(25, parrafo);
	//Respecto al Punto 1
	cout << setw(8) << " "
		 << "(" << controlPoints[1][0] << "," << controlPoints[1][1] << ")" << setfill(' ') << endl;
	parrafo++;
}

// Funcion de procesamiento de entrada de teclado.
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	case ' ':
		if (numVal < 3)
		{
			numVal++;
			if (numVal == 1 || numVal == 3)
				tablaDatos();
		}
		else
			numVal = 0;
		glutPostRedisplay(); //Esta funci�n env�a un evento de redibujado.
		break;
	default:
		break;
	}
}

// Funcion de devoluci�n de llamada para entrada de clave no ASCII.
//Donde cuyos argumentos sean la tecla que se oprimira, y las coordenadas
//en x e y ,utilizamos else e if, los valores que puede tomar la
//variable key seran:
//GLUT_KEY_RIGHT        ->         flecha derecha
//GLUT_KEY_LEFT         ->         flecha izquierda
//GLUT_KEY_UP           ->         flecha arriba
//GLUT_KEY_DOWN         ->         flecha abajo
void specialKeyInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		if (numVal % 2 == 0)
			controlPoints[numVal / 2][1] += 0.5;
		else
			tangentVectors[numVal / 2][1] += 0.5;
	}
	if (key == GLUT_KEY_DOWN)
	{
		if (numVal % 2 == 0)
			controlPoints[numVal / 2][1] -= 0.5;
		else
			tangentVectors[numVal / 2][1] -= 0.5;
	}
	if (key == GLUT_KEY_LEFT)
	{
		if (numVal % 2 == 0)
			controlPoints[numVal / 2][0] -= 0.5;
		else
			tangentVectors[numVal / 2][0] -= 0.5;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		if (numVal % 2 == 0)
			controlPoints[numVal / 2][0] += 0.5;
		else
			tangentVectors[numVal / 2][0] += 0.5;
	}
	glutPostRedisplay(); //Esta funci�n env�a un evento de redibujado.
}

// Funcion para generar instrucciones de interacci�n en la ventana de C ++.
void printInteraction(void)
{
	float x0, x1, y0, y1;
	cout << "Interaccion:" << endl;
	cout << " - Presiona la tecla espacio para seleccionar un punto de control o un vector tangente" << endl;
	cout << " - Presione las teclas de direccion para mover el punto de control o el vector tangente" << endl;
	cout << "" << endl;
	//Se ingresa por teclado los datos que se desee para cada uno de los dos puntos iniciales respecto al eje X e Y
	cout << "INGRESE LOS PUNTOS DE CONTROL:" << endl;
	cout << "--- Punto 0:" << endl;
	cout << "  * X: ";
	cin >> x0;
	cout << "  * Y: ";
	cin >> y0;
	cout << "--- Punto 1:" << endl;
	cout << "  * X: ";
	cin >> x1;
	cout << "  * Y: ";
	cin >> y1;
	controlPoints[0][0] = x0;
	controlPoints[0][1] = y0;
	controlPoints[0][2] = 0.0;
	controlPoints[1][0] = x1;
	controlPoints[1][1] = y1;
	controlPoints[1][2] = 0.0;
	//Tabla Inicial que muestra datos de los recorridos que hacen los Puntos 0 y 1
	cout << "------------ TABLA DE DATOS ------------" << endl;
	cout << right << setw(17) << "Punto 0" << setw(23) << "Punto 1" << endl;
	cout << setfill('-') << setw(40) << "-" << endl;
	cout << setw(10) << setfill(' ') << " "
		 << "(" << controlPoints[0][0] << "," << controlPoints[0][1] << ")";
	gotoxy(25, 14);
	cout << setw(8) << setfill(' ') << " "
		 << "(" << controlPoints[1][0] << "," << controlPoints[1][1] << ")" << endl;
}

// Rutina principal.
int main(int argc, char **argv)
{
	printInteraction();									//Llamar a la funcion printInteraction()
	glutInit(&argc, argv);								//Lo primero que debemos mandar llamar en un proyecto con la libreria GLUT
														// &argc -> hace referencia al espacio de memoria que es la cantidad de elementos que se le colocan en la terminal
														// argv -> lista de elementos con la cual la funcion main se invoca (-lglu32 -lglut32 -lopengl32 -lstdc++)
	glutInitContextVersion(4, 3);						//Para forzar al contexto opengl a usar una cierta versi�n de opengl
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE); //Para forzar al contexto opengl a usar un cierto perfil del opengl
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);		// Define el modo en que se mostrara la ventana principal de nuestro proyecto,
														// las constantes predefinidas para el color son:
														// GLUT_RGBA -> selecciona una ventana RGBA(Red,Green, Blue, Alpha). El cual es el color por defecto.

	// El modo mostrar tambien nos permite seleccionar una ventana de un unico o doble buffer, las constantes son:
	// GLUT_SINGLE - Un unico buffer.
	// GLUT_DOUBLE - Una ventana de doble buffer. Nos permite teneranimaciones suaves (El cual en este ejercicio usaremos)
	glutInitWindowSize(500, 500);		  //Permite definir el tama�oo de la ventanaprincipal de nuestro proyecto.
	glutInitWindowPosition(100, 100);	  // Permite definir donde se encontrara la esquina superior izquierda de la ventana
										  //principal de nuestro proyecto
	glutCreateWindow("hermiteCubic.cpp"); //tiulo que tendra nuestro programa
	glutDisplayFunc(drawScene);			  //Esta funci�n es la que GLUT ejecutar� cada vez que haya que redibujar el contenido
	glutReshapeFunc(resize);			  //Esta funci�n es la que GLUT ejecutar� cada vez que la ventana cambie de tama�o
	glutKeyboardFunc(keyInput);			  //establece la devoluci�n de llamada del teclado para la ventana actual
	glutSpecialFunc(specialKeyInput);	  //Para utilizar las teclas de flecha para manipular objetos dentro del programa

	//glewExperimental = GL_TRUE; //para usar funciones no incluidas en opengl.lib
	glewInit(); //Inicializa GLEW

	setup();		//llamar a la funcion setup()
	glutMainLoop(); //Esta funci�n provocar� que todas las funciones GLUT que controlan funciones,
					//se repitan cuando sea necesario, es decir, es un bucle inteligente, en otras
					///palabras, es darle a GLUT el control sobre el programa, algo que no es muy
					//com�n en las librer�as t�picas de C.
}

// g++ -m32 -Wall -o 2.out 2.cpp -L"C:\MinGW\lib" -lglu32 -lglut32 -lopengl32 -lstdc++